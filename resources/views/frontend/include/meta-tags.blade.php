@if( Route::currentRouteName() == 'category.blogs' || Route::currentRouteName() == 'blog.show')
    <title>@yield('title')</title>
    <meta name="description" content="@yield('description')">
    <meta name="keywords" content="@yield('keywords')">
    <link rel="canonical" href="@yield('canonical')">
    <meta property="og:locale" content="@yield('og-locale')"/>
    <meta property="og:type" content="@yield('og-type')"/>
    <meta property="og:title" content="@yield('og-title')"/>
    <meta property="og:description" content="@yield('og-description')"/>
    <meta property="og:url" content="@yield('og-url')"/>
    <meta property="og:site_name" content="@yield('og-site-name')"/>
    <meta property="og:image" content="@yield('og-image')"/>
@elseif(is_null($meta_information))
    <title>SolarNest | @yield('title')</title>
    <meta name="description"
          content="SolarNest">
    <meta name="keywords" content="SolarNest">
    <meta name="robots" content="noindex"/>
    <link rel="canonical" href="@yield('canonical')">
    <meta property="og:locale" content="@yield('og-locale')"/>
    <meta property="og:type" content="@yield('og-type')"/>
    <meta property="og:title" content="@yield('og-title')"/>
    <meta property="og:description" content="@yield('og-description')"/>
    <meta property="og:url" content="@yield('og-url')"/>
    <meta property="og:site_name" content="@yield('og-site-name')"/>
    <meta property="og:image" content="@yield('og-image')"/>
@else
    <title>{{$meta_information->title}}</title>
    <meta name="description" content="{{ $meta_information->description }}">
    <meta name="keywords" content="{{ $meta_information->keywords }}">
    <link rel="canonical" href="@yield('canonical')">
    <meta property="og:locale" content="@yield('og-locale')"/>
    <meta property="og:type" content="@yield('og-type')"/>
    <meta property="og:title" content="@yield('og-title')"/>
    <meta property="og:description" content="@yield('og-description')"/>
    <meta property="og:url" content="@yield('og-url')"/>
    <meta property="og:site_name" content="@yield('og-site-name')"/>
    <meta property="og:image" content="@yield('og-image')"/>



@endif


