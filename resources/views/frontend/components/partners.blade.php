<div class="partners_Wrap">
    <div class="row">
        <div class="col-md-12 col-xl-6 col-lg-6 col-sm-12 col-12 pl-0 pr-0">
            <div class="img_bannerr">
                <img class="lozad" data-src="{{ asset('frontend/images/partners_banner.png') }}" alt="solar panel installation services">
            </div>
        </div>
        <div class="col-md-12 col-xl-6 col-lg-6 col-sm-12 col-12 pl-0 pr-0">
            <div class="become_partner_wrap"
                 >
                <div class="partners_text" style="background-image: url({{ asset('frontend/images/Partnertxt-bg.png') }});">
                    <h3>Partner with us</h3>
                    <p>SolarNest is an ever-growing solar panel marketplace where buyers meet sellers. Join our rapid and diverse seller community to increase your sales and elevate your market share. </p>
                    <p>Get in touch today!</p>
                    <div class="learn_more ">
                        <a class="learn_more_btn fill-border-btn" href="{{ route('become.partner') }}"><span>Learn More<i
                                    class="fa fa-angle-right"></i></span> </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if(!empty($partners) && count($partners) > 0)
        <div class="partners_listing_wrap"
             style="background-image: url({{ asset('frontend/images/Partner-bg.png') }});">
            <div class="partners_listing">
                <!-- <div class="owl-carousel partnersCarousal" id="client-logos">
                    @foreach($partners as $partner)
                        <div class="partner">
                            <img src="{{ asset('/uploads/partners_logos/'.$partner->logo) }}">
                        </div>
                    @endforeach
                </div> -->
                <!-- <div id="client-logos" class="owl-carousel text-center">
                    @foreach($partners as $partner)
                    <div class="item">
                        <div class="client-inners">
                            <img src="{{ asset('/uploads/partners_logos/'.$partner->logo) }}">
                        </div>
                    </div>
                    @endforeach
                </div> -->
                <div class="exsitingPartnersCarousel">
                @foreach($partners as $partner)
                    <div class="slick-slide">
                        <div class="item">
                            <div class="client-inners">
                                <img src="{{ asset('/uploads/partners_logos/'.$partner->logo) }}" alt="solar companies in Lahore">
                            </div>
                        </div>
                    </div>   
                    @endforeach
                </div>
            </div>
        </div>
    @endif
</div>
