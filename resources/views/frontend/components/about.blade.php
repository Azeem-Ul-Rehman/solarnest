@push('css')
    <style>
        /*.testimonial_wrapper .background_text .about_text .block_quote {*/
      
        /*    display: block !important;*/
        /*    font-size: 37px;*/
        /*    font-weight: 700;*/
        /*    line-height: 58px;*/
        /*}*/
        
        
    
        /*.testimonial_wrapper .background_text .about_text .block_quote .block_quote_span {*/
        /*    height: auto !important;*/
        /*    width:auto !important;*/
        /*    display: inline-block !important;*/
        /*    font-size: 70px;*/
        /*}*/
        
    </style>
@endpush
<div class="testimonial_wrapper">
    <div class="row">
        <div class="col-md-12 col-lg-6 col-xl-6 col-12 pl-0 pr-0">
            <div class="testimonial_img">
                <img class="lozad" data-src="{{ asset('frontend/images/solar-panel.png') }}" alt="solar energy installation company">
            </div>
        </div>
        <div class="col-md-12 col-lg-6 col-xl-6 col-12 pl-0 pr-0">
            <div class="background_text"
            >
                <!-- <img src="images/AboutUs-bg.png"> -->
                <section>
                    <div class="about_text lozad" data-background-image="{{ asset('frontend/images/AboutUs-bg.png') }}" >
                        <h2 class="block_quote">
                           <span>"</span>Step into a sustainable<br> future of solar power
                        </h2>
                        <p>{{ $about->summary }}
                        </br>
                        </br>
                        All you have to do is simply request for offers on our website by submitting your requirements. Our system will evaluate your requirements, to provide you the best offer comparison according to your location and needs. You can then select an installer and start your solar system installation process!

                        </p>
                        <div class="learn_more ">
                            <a class="learn_more_btn fill-border-btn" href="{{ route('about-us') }}"><span>Learn More<i
                                        class="fa fa-angle-right"></i></span>
                            </a>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>
