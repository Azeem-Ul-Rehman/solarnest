@push('css')
    <style>
        a{
            color:#003466;
        }
        a:hover{
            color:#003466;
        }
    </style>
@endpush
<div class="contact_area_footer">
    <div class="contact_footer_inner" >
        <div class="container">
            <div class="contact_field">
                <h3 style="background-image: url({{ asset('frontend/images/footer-Contact.png') }});">Get In Touch</h3>
                <div class="contact_info_block">
                    <div class="contact_info">
                        <span>
                            <i class="fa fa-phone"></i>
                            <h5><a href="tel:{{ $settings->contact_number }}">{{ $settings->contact_number }}</a></h5>
                        </span>
                    </div>
                    <div class="contact_info">
                        <span>
                            <i class="fa fa-envelope"></i>
                            <h5><a href="mailto:{{ $settings->email }}">{{ $settings->email }}</a></h5>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
