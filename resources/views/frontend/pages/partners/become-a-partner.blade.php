@extends('frontend.layout.app')
@section('title','Become a Partner')
@section('canonical','https://solarnest.pk')
@section('og-locale','en_US')
@section('og-type','website')
@section('og-title','SolarNest')
@section('og-description','We at SolarNest believe clean energy and storage should be accessible to everybody.  We intend to transform the relationship between Solar Energy Installers and the consumers who want to install solar energy system.')
@section('og-url','https://solarnest.pk')
@section('og-site-name','SolarNest')
@section('og-image','https://solarnest.pk/frontend/images/slide1.jpg')
@push('css')
    <style>
        .invalid-feedback {
            display: block !important;
            margin-top: -0.75rem !important;
        }
        .become-partner-content {
  margin-top: 70px;
  padding: 0px 40px;
}
.become-partner-content h1 {
  font-size: 40px;
  font-weight: 700;
}
.become-partner-content p {
  color: #4A4A4A;
}
.become-partner-content .becomne-partner-keypoints .keypoint-listing {
  width: 100%;
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
}
.become-partner-content .becomne-partner-keypoints .keypoint {
  background: #003466;
  color: #fff;
  padding: 30px;
  background-repeat: no-repeat;
  background-position: 95% -3%;
  background-size: 20%;
  margin-bottom: 30px;
  width: 49%;
  align-items: end;
  flex-flow: column;
  display: flex;
  justify-content: center;
}
.become-partner-content .becomne-partner-keypoints .keypoint h3 {
  font-size: 22px;
  font-weight: bold;
  color: #fff;
  width: 79%;
}
.become-partner-content .becomne-partner-keypoints .keypoint p {
  margin-top: 15px;
  font-size: 14px;
  color: #fff;
}
.become-partner-content .becomne-partner-keypoints .keypoint:nth-child(1) {
  background-image: url(https://solarnest.pk/frontend/images/01.png);
}
.become-partner-content .becomne-partner-keypoints .keypoint:nth-child(2) {
  background-image: url(https://solarnest.pk/frontend/images/02.png);
}
.become-partner-content .becomne-partner-keypoints .keypoint:nth-child(3) {
  background-image: url(https://solarnest.pk/frontend/images/03.png);
}
.become-partner-content .becomne-partner-keypoints .keypoint:nth-child(4) {
  background-image: url(https://solarnest.pk/frontend/images/04.png);
}
.become-partner-content .becomne-partner-keypoints .keypoint:nth-child(5) {
  background-image: url(https://solarnest.pk/frontend/images/05.png);
}
.become-partner-content .becomne-partner-keypoints .keypoint:nth-child(6) {
  background-image: url(https://solarnest.pk/frontend/images/06.png);
}
@media screen and (max-width: 600px) {
    .become-partner-content .becomne-partner-keypoints .keypoint{
        width:100%
    }
}
    </style>
@endpush
@section('content')
    <div class="become-partner-wrapper" style="background-image: url({{ asset('frontend/images/faded-bg.png') }});">
        <div class="container">
            <div class="become-partner-inner">
                <div class="row flex-wrap-revs">
                    <div class="col-md-12 col-lg-7 col-xl-7 col-12">
                        <div class="become-partner-form-wrap"
                             style="background-image: url({{ asset('frontend/images/Partner2-bg.png') }});">
                            <div class="title">
                                <h3>Become a Partner</h3>
                                <p><span>*</span>Required fields</p>
                            </div>
                            <p>Are you an installer who wants to partner up with us to increase your market share and
                                help
                                create a sustainable energy ecosystem?</p>
                            <div class="become_partner_form">
                                <div class="form">
                                    <form action="{{ route('save.partner') }}" method="POST"
                                          enctype="multipart/form-data">
                                        @csrf
                                        <div class="row">
                                            <div class="col-md-6 col-lg-6 col-xl-6 col-12">
                                                <div class="form-colm @error('first_name') is-invalid @enderror">
                                                    <input placeholder="First name*" id="first_name" type="text"
                                                           value="{{ old('first_name') }}"
                                                           name="first_name">
                                                </div>
                                                @error('first_name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                            <div class="col-md-6 col-lg-6 col-xl-6 col-12">
                                                <div class="form-colm @error('last_name') is-invalid @enderror">
                                                    <input placeholder="Last Name*" id="last_name" type="text"
                                                           value="{{ old('last_name') }}"
                                                           name="last_name">
                                                </div>
                                                @error('last_name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6 col-lg-6 col-xl-6 col-12">
                                                <div class="form-colm @error('company_name') is-invalid @enderror">
                                                    <input placeholder="Company name*" id="company_name" type="text"
                                                           value="{{ old('company_name') }}"
                                                           name="company_name">
                                                </div>
                                                @error('company_name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                            <div class="col-md-6 col-lg-6 col-xl-6 col-12">
                                                <div class="form-colm @error('phone_number') is-invalid @enderror">
                                                    <input
                                                        name="phone_number" value="{{ old('phone_number') }}"
                                                        autocomplete="phone_number"
                                                        placeholder="Phone Number*"
                                                        pattern="[03]{2}[0-9]{9}"
                                                        maxlength="11"
                                                        onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')"
                                                        title="Phone number with 03 and remaing 9 digit with 0-9">
                                                </div>
                                                @error('phone_number')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                            {{--                                            <div class="col-md-6 col-lg-6 col-xl-6 col-12">--}}
                                            {{--                                                <div class="form-colm  @error('cnic') is-invalid @enderror">--}}
                                            {{--                                                    <input placeholder="_____-_______-_" id="cnic" name="cnic"--}}
                                            {{--                                                           type="text"--}}
                                            {{--                                                           class="cnic-mask">--}}
                                            {{--                                                </div>--}}
                                            {{--                                                @error('cnic')--}}
                                            {{--                                                <span class="invalid-feedback" role="alert">--}}
                                            {{--                                                    <strong>{{ $message }}</strong>--}}
                                            {{--                                                </span>--}}
                                            {{--                                                @enderror--}}
                                            {{--                                            </div>--}}
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12 col-lg-12 col-xl-12 col-12">
                                                <div class="form-colm  @error('address') is-invalid @enderror">
                                                    <input placeholder="Address (optional)" id="address" type="text"
                                                           name="address" value="{{ old('address') }}">
                                                </div>
                                                @error('address')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="row">

                                            <div class="col-md-12 col-lg-12 col-xl-12 col-12">
                                                <div class="form-colm @error('email') is-invalid @enderror">
                                                    <input placeholder="Email Address*" id="email" type="email"
                                                           name="email"
                                                           value="{{ old('email') }}">
                                                </div>
                                                @error('email')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="col-md-12 col-lg-12 col-xl-12 col-12">
                                            <div class="become-partner">
                                                <button class="become-partner-btn fill-border-btn" type="submit">
                                                <span>Become Partner<i
                                                        class="fa fa-angle-right"></i></span>
                                                </button>
                                            </div>
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-5 col-xl-5 col-12 pr-0 pl-0">
                        <div class="become_partner_cover">
                            <img src="{{ asset('frontend/images/solar1.png') }}" alt="best solar company in Pakistan">
                        </div>
                    </div>
                </div>
            </div>

            <!-- become partner new section -->
            <div class="become-partner-content">
                        <h1>Become a Partner</h1>
                        <p>
                            SolarNest is a platform designed to promote sustainability in Pakistan’s energy sector by promoting the use of Solar Energy. We do this by bringing your Solar Panels Business and the people looking for Solar Panel Systems together. To put it simply, we connect people looking for Solar Panel Installation with the right Solar Panels Company in Pakistan for them.
                        </p>
                        <p>
                            Our process is quite simple, designed to ensure maximum engagement. Once a client decides to purchase Solar Panel Systems and is looking for the right Solar Panels Company in Pakistan, they head over to our website and fill in their details. Once they do this, quotes from our partners are made visible to the client. The client then chooses the Solar Panels Business that provides the best quote according to them and that partner gets to send the client a detailed proposal. Once the proposal is accepted, you can move ahead with the home visit and finalize the sale!
                        </p>
                        <div class="becomne-partner-keypoints">
                            <div class="row">
                                <div class="keypoint-listing">
                                    <div class="keypoint">
                                        <h3>Lower Your Business Development and Sales Costs</h3>
                                            <p>Everyone visiting our marketplace is a potential client for you. Any marketing of our marketplace is marketing for you too! With us attracting people looking for Solar Panel Systems and Solar Panel Installation services, we will be generating multiple leads for you to pitch to with your quote and become the right Solar Panels Company in Pakistan for them
                                            </p>
                                            <p>Every lead that we generate for you, you save the Business Development, Marketing, and negotiation effort costs that you otherwise would have had to spend on generating the lead yourself. Therefore, partnering with us helps you cut costs too!
                                            </p>
                                    </div>
                                    <div class="keypoint">
                                        <h3>Get Featured on a Digital Platform</h3>
                                            <p>All of our partner Solar Panels companies in Pakistan get featured on our website! This enables you to be seen by all the people looking for Solar Panel Systems and Solar Panel installation services. Additionally, this also offers free marketing to your Solar Panels Business by increasing your visibility. Moreover, client reviews on our site help you distinguish yourself in the marketplace as well. 
                                            </p>
                                    </div>
                                    <div class="keypoint">
                                        <h3>Capture More Quality Leads</h3>
                                            <p>All the leads that SolarNest provides are from people who are actually willing to purchase Solar Panel Systems and avail Solar Panel installation services. Therefore, they are actively looking for a Solar Panels business and this enables our partner Solar Panels companies in Pakistan to avail quality leads with high conversion rates.
                                            </p>
                                    </div>
                                    <div class="keypoint">
                                        <h3>Increase Your Reach to Different Cities with SolarNest</h3>
                                            <p>SolarNest is a digital platform intending to help solar panel companies in Pakistan connect with potential customers. By partnering with our company, your Solar Panels business profile would be visible to people across the entire country without any additional costs. We can be the break you need to spread your Solar Panel installation services throughout the nation. Even if you're situated in a small town, we can help you reach clients, investors, and customers in major cities. 
                                            </p>
                                    </div>
                                    <div class="keypoint">
                                        <h3>Target Your Desired Market</h3>
                                            <p>We at SolarNest believe in quality leads. As such, our target audience is made up of people who are or might become interested in Solar Panel Systems and who require or might require Solar Panel Installation services. With us, your Solar Panels business can gain access to this market segment. Additionally, you send the detailed proposal once a client picks your quote, giving you the power to manage client relations. 
                                            </p>
                                    </div>
                                    <div class="keypoint">
                                        <h3>Increase your Sales</h3>
                                            <p>As discussed above, SolarNest offers our partner Solar Panel Companies in Pakistan with quality leads that ensure higher conversation rates. 
                                            </p>
                                            <p>With SolarNest providing you people looking for Solar Panel systems and Solar Panel installation services; you have a chance to boost your sales exponentially!
                                            </p>
                                            <p>So join us today as a partner Solar Panels business and take advantage of the numerous qualitative and quantitative benefits we have to offer!
                                            </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

        </div>
    </div>
    @include('frontend.components.contact')
@endsection
@push('js')
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"></script>
    <!--end::Page Scripts -->
    <script type="text/javascript">

        $(document).ready(function () {
            if ($('.cnic-mask').length) {
                $('.cnic-mask').mask('00000-0000000-0');
            }
            $('#cnic').focusout(function () {
                cnic_no_regex = /^[0-9+]{5}-[0-9+]{7}-[0-9]{1}$/;

                if (cnic_no_regex.test($(this).val())) {
                } else {

                    $(this).val('');
                }
            });
            $('#phone_number').focusout(function () {
                if (/^(03)\d{9}$/.test($(this).val())) {
                    // value is ok, use it
                } else {

                }
            });

        });
    </script>

@endpush

