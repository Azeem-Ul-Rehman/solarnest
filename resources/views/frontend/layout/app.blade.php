<!DOCTYPE html>
<html lang="en">
<head>

@include('frontend.include.meta-tags')
<meta name="facebook-domain-verification" content="px71zz2m9q9uc9sgjxn0beov89kjcq" />
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-N5C986F');</script>
    <!-- End Goog --->
<!-------- font awesome-->
    <link rel="stylesheet" href="{{ asset('frontend/css/font-awesome.min.css') }}">
    <!-------- FAVICON-->
    <link rel="icon" type="image/png" href="{{ asset('favicon.png') }}">
    <link rel="icon" type="image/x-icon" href="{{ asset('favicon.ico') }}">
    <link rel="apple-touch-icon-precomposed" href="">
    <link rel="msapplication-TileImage" href="">
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Bootstrap CSS -->
    <link rel="shortcut icon" href="" type="image/x-icon">
{{--    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">--}}
    {{--    <link rel="stylesheet" href="{{ asset('frontend/css/bootstrap/bootstrap.css') }}">--}}

    <link rel="preload" as="font" href="{{ asset('frontend/fonts/Exo-Regular.woff') }}" type="font/woff2"
          crossorigin="anonymous">
    <link rel="preload" as="font" href="{{ asset('frontend/fonts/Exo-Regular.woff2') }}" type="font/woff2"
          crossorigin="anonymous">
        <link rel="stylesheet" href="{{ asset('frontend/css/slick.css') }}">
        <link rel="stylesheet" href="{{ asset('frontend/css/slick-theme.css') }}">
    {{--    <link rel="stylesheet" href="{{ asset('frontend/css/style.css') }}">--}}
    {{--    <!-- Toastr -->--}}
    {{--    <link rel="stylesheet" href="{{ asset('frontend/css/toastr.min.css') }}">--}}

    <link rel="stylesheet" href="{{ asset('frontend/css/app.css') }}">

    @stack('css')
    <!-- Facebook Pixel Code -->
        <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window, document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '361741342027903');
        fbq('track', 'PageView');
        </script>
        <noscript><img height="1" width="1" style="display:none"
        src="https://www.facebook.com/tr?id=361741342027903&ev=PageView&noscript=1"
        /></noscript>
    <!-- End Facebook Pixel Code -->
</head>
<body>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-N5C986F"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
<div class="wrapper">
    @if( Route::currentRouteName() != 'login')
        @include('frontend.include.header')
    @endif
    @yield('content')
    @if( Route::currentRouteName() != 'login')
        @include('frontend.include.footer')
    @endif
</div>

<script src="{{ asset('js/app.js') }}"></script>
{{--<script src="{{ asset('frontend/js/jquery-3.3.1.min.js') }}"></script>--}}
{{--<script src="{{ asset('frontend/js/jquery-ui.js') }}"></script>--}}
{{--<script src="{{ asset('frontend/js/bootstrap.js') }}"></script>--}}
{{--<script src="{{ asset('frontend/js/owl.carousel.js') }}"></script>--}}
{{--<!-- Toastr -->--}}
{{--<script src="{{ asset('frontend/js/toastr.min.js') }}"></script>--}}
{{--<script src="{{ asset('frontend/js/custom.js') }}"></script>--}}
<script src="{{ asset('frontend/js/slick.min.js') }}"></script>

@stack('models')
@stack('js')
{{--<!-- IntersectionObserver polyfill -->--}}
{{--<script src="https://raw.githubusercontent.com/w3c/IntersectionObserver/master/polyfill/intersection-observer.js"></script>--}}

<!-- Lozad.js from CDN -->
<script src="https://cdn.jsdelivr.net/npm/lozad"></script>
<script>

    // Initialize library
    lozad('.lozad').observe()


    @if(Session::has('flash_message'))
    var type = "{{ Session::get('flash_status') }}";
    switch (type) {
        case 'info':
            toastr.info("{{ Session::get('flash_message') }}");
            break;

        case 'warning':
            toastr.warning("{{ Session::get('flash_message') }}");
            break;

        case 'success':
            toastr.success("{{ Session::get('flash_message') }}");
            break;

        case 'error':
            toastr.error("{{ Session::get('flash_message') }}");
            break;
    }
    @endif

</script>

</body>
</html>
