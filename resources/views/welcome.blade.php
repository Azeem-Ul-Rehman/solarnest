@extends('frontend.layout.app')
@section('canonical','https://solarnest.pk')
@section('og-locale','en_US')
@section('og-type','website')
@section('og-title','SolarNest')
@section('og-description','We at SolarNest believe clean energy and storage should be accessible to everybody.  We intend to transform the relationship between Solar Energy Installers and the consumers who want to install solar energy system.')
@section('og-url','https://solarnest.pk')
@section('og-site-name','SolarNest')
@section('og-image','https://solarnest.pk/frontend/images/slide1.jpg')

@push('css')
    <style>
        .co-tagline {
            color: #fff;
            font-size: 37px;
        }

        .overlay_content h1 {
            font-size: 7rem;
            color: #fff;
            font-weight: 800;
        }

        .benefit {
            color: #fff;
            font-size: 20px;
        }

        .tagline_banner h3 {
            color: #fff;
            font-size: 18px;
        }

        /* Slider */

        .exsitingPartnersCarousel {
            width: 100%;
            margin: 0px auto;
            display: flex;
            justify-content: flex-start !important;
        }

        .slick-slide {
            margin: 10px;
        }

        .slick-slide img {
            width: 100%;
        }

        .slick-prev, .slick-next {
            background: transparent;
            border-radius: 15px;
            border-color: transparent;
        }

        .slick-next:before, .slick-prev:before {
            color: #000;
        }

        .slick-initialized .slick-slide {
            display: block;
            min-width: 250px !important;
        }

        .slick-list {
            display: contents;
        }

        .slick-track {
            min-width: 100%;
        }

        .slick-slide {
            min-width: 250px !important;
        }
        h2{
            text-align:center;
            padding: 20px;
        }
        .title h2{
            text-align:left;
        }
/* Slider */

.slick-slide {
    margin: 0px 20px;
}

.slick-slide img {
    width: 100%;
}

.slick-slider
{
    position: relative;
    display: block;
    box-sizing: border-box;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
            user-select: none;
    -webkit-touch-callout: none;
    -khtml-user-select: none;
    -ms-touch-action: pan-y;
        touch-action: pan-y;
    -webkit-tap-highlight-color: transparent;
}

.slick-list
{
    position: relative;
    display: block;
    overflow: hidden;
    margin: 0;
    padding: 0;
}
.slick-list:focus
{
    outline: none;
}
.slick-list.dragging
{
    cursor: pointer;
    cursor: hand;
}

.slick-slider .slick-track,
.slick-slider .slick-list
{
    -webkit-transform: translate3d(0, 0, 0);
       -moz-transform: translate3d(0, 0, 0);
        -ms-transform: translate3d(0, 0, 0);
         -o-transform: translate3d(0, 0, 0);
            transform: translate3d(0, 0, 0);
}

.slick-track
{
    position: relative;
    top: 0;
    left: 0;
    display: block;
}
.slick-track:before,
.slick-track:after
{
    display: table;
    content: '';
}
.slick-track:after
{
    clear: both;
}
.slick-loading .slick-track
{
    visibility: hidden;
}

.slick-slide
{
    display: none;
    float: left;
    height: 100%;
    min-height: 1px;
}
[dir='rtl'] .slick-slide
{
    float: right;
}
.slick-slide img
{
    display: block;
}
.slick-slide.slick-loading img
{
    display: none;
}
.slick-slide.dragging img
{
    pointer-events: none;
}
.slick-initialized .slick-slide
{
    display: block;
}
.slick-loading .slick-slide
{
    visibility: hidden;
}
.slick-vertical .slick-slide
{
    display: block;
    height: auto;
    border: 1px solid transparent;
}
.slick-arrow.slick-hidden {
    display: none;
}
.item{
     height: 100%;
    width: 100%;
    display: inline-block !important;
}
.slick-track{
    display:flex;
}

        @media screen and (max-width: 600px) {
            .overlay_content h1 {
                font-size: 44px !important;
            }

            .co-tagline {
                font-size: 20px;
                margin-top: 20px;
            }

            .benefit {
                font-size: 14px;
            }

            .tagline_banner h3 {
                font-size: 11px;
            }

            .owl-stage {
                justify-content: flex-start;
                align-items: center;
            }
        }

        /* .partner{
            display: flex;
            align-items: center;
        }
        .overlay_content h1{
            font-size: 50px !important;
            text-align: center;
            margin-bottom: 30px !important;
            line-height: 50px !important;
            padding: 0px 250px;
        }
        .overlay_content .co-tagline{
            color: #fff;
            font-size: 37px;
            font-weight: 500;
            line-height: 1.2;
            margin-bottom: 50px;
        }
        .overlay_content .benefit{
            color: #fff;
            font-size: 20px;
            font-weight: 500;
            margin-bottom: 1rem;
            line-height:20px;
        }
        .tagline_banner h3{
            color: #fff;
            font-size: 20px;
            font-weight: 500;
            margin-bottom: 1rem;
            line-height:20px;
            text-align:center;
        } */
        /* @media screen and (max-width: 600px) {
            .overlay_content h1{
                padding: 0px 34px !important;
                line-height: 40px !important;
                margin-bottom: 40px !important;
                font-size: 1.6rem !important;
            }
            .overlay_content .co-tagline{
                font-size: 24px !important;
                margin-bottom: 50px;
                padding: 0px 20px;
                text-align: center;
            }
            .benefit{
                font-size: 16px;
                padding: 0px 20px;
                text-align: center;
            }
            .main_slider {
                height: 93vh;
            }
            .main_slider .carousel-inner {
                height: 93vh;
            }
            .overlay_content .benefit{
                font-size: 14px !important;
            }
            .tagline_banner h3{
                font-size: 14px !important;
            }
        }
        @media screen and (max-width: 768px) {
            .overlay_content h1{
                padding: 0px 34px !important;
            }
        } */

    </style>
@endpush
@section('content')
    <div class="content_wrapper">
        <!-- main slider  -->
    @include('frontend.components.main-banner')
    <!-- main slider end-->
        <!-- about section -->
    @include('frontend.components.about')
    <!-- about section end-->
        <!-- how we work wap -->
    @include('frontend.components.info-graphic')
    <!-- how we work wap end-->
        <!-- partners wrap -->
    @include('frontend.components.partners')
    <!-- partners wrap end -->
        <!-- blog wrap start -->
    @include('frontend.components.blog')
    <!-- blog wrap end-->
        <!-- contact wrap start -->
    @include('frontend.components.contact')
    <!-- contact wrap end-->
    </div>
@endsection
@push('js')
    <script>

        $(document).ready(function () {
            $('.exsitingPartnersCarousel').slick({
                speed: 500,
                slidesToShow: 4,
                slidesToScroll: 1,
                autoplay: false,
                autoplaySpeed: 2000,
                dots: false,
                // centerMode: false,
                responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1,
                        // centerMode: true,

                    }

                }, {
                    breakpoint: 800,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                        dots: false,
                        infinite: true,

                    }
                }, {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        dots: false,
                        infinite: true,
                        autoplay: true,
                        autoplaySpeed: 2000,
                    }

                }]
            });
        });

    </script>
@endpush
